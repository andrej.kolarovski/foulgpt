const HIVONISMS = {
  "fullSayings": [
    "затни се",
    "wow чоек олабави малку",
    "уќути се глупаку",
    "Каков предосаден глупак господе земи ме",
    "Убиј се",
    "... sad",
    "Оф.. тажно суштество",
    "ако те игнорирам дали ќе прекинеш?",
    "Гррррррррр",
    "хахах одвратно",
    "Imam dobro srce",
    "Ološu zaveži usta",
    "Леле...",
    "хахаха глупо едно",
    "врло смешно, чекај да се насмеам со капс \"XAXAXA\"",
    "тебе на улица да те видам легнат ќе те прескокнам",
    "Мрш",
    "U boring piece of shit",
    "ај ебете се глупи чао ",
    "не",
    "Неочекувано од тебе ама фајн",
    "Имам добро срце",
    "греота",
    "калаштурo!",
    "одвратна екипица",
    "Шо без мене бе?",
    "ќе ти го скршам вратчето",
    "ќе се воздржам до коментари..",
    "Фацата ти е промашај",
    "Иу",
    "Леле... УЖАС!",
    ":)",
    "не ме замарај",
    "трагично е колку си опседнат со мене а јас стварно не сакам да те дружам.. сори :(",
    "a, не прочитав сори",
    "Ццц!!!",
  ],
  "partialComebacks": [
    {
      "comeback": "Фацата ти е ",
      "placeAt": "startOfSentence"
    },
  ],
  "personalizedSayings": [
    {
      "comeback": "скини ми се с курца педер",
      "placeNameAt": "startOfSentence",
    },
    {
      "comeback":  "Светски ден на заќути",
      "placeNameAt": "endOfSentence",
    },
    {
      "comeback": "греота си",
      "placeNameAt": "startOfSentence",
    },
    {
      "comeback": "кретену еден глупав",
      "placeNameAt": "startOfSentence",
    },
  ]
};

const partialComebacksTriggerWords = ["глуп", "глупа", "глупо", "грд", "грда", "грдо", "смешен", "смешна", "смешно", "смотан", "смотана", "смотано"];

let genericReplyStack = [];
let reuseWordReplyStack = [];
let personalizedReplyStack = [];
let chatStack = [];
let username;


function deepCopy(object) {
  return JSON.parse(JSON.stringify(object));
}

function stir(array) {
  let currentIndex = array.length,  randomIndex;

  // While there remain elements to shuffle.
  while (currentIndex != 0) {

    // Pick a remaining element.
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex--;

    // And swap it with the current element.
    [array[currentIndex], array[randomIndex]] = [
      array[randomIndex], array[currentIndex]];
  }

  return array;
}

function initStacks() {
  genericReplyStack = deepCopy(HIVONISMS.fullSayings);
  reuseWordReplyStack = deepCopy(HIVONISMS.partialComebacks);
  personalizedReplyStack = deepCopy(HIVONISMS.personalizedSayings);
  chatStack = JSON.parse(localStorage.getItem('hgpt-chatstack')) || [];
}

function mapPartialComeback(triggerWord) {
  const index = partialComebacksTriggerWords.indexOf(triggerWord)
  let responseWord;

  if ((index + 1) % 3 === 0) {
    responseWord = partialComebacksTriggerWords[index - 1];
  }

  if (index % 3 === 0) {
    responseWord = partialComebacksTriggerWords[index + 1];
  }

  if ((index - 1) % 3 === 0) {
    responseWord = partialComebacksTriggerWords[index];
  }

  if (!reuseWordReplyStack.length)
    reuseWordReplyStack = deepCopy(HIVONISMS.partialComebacks);

  reuseWordReplyStack = stir(reuseWordReplyStack);
  const response = reuseWordReplyStack.pop();
  let ret;

  switch (response.placeAt) {
    case 'startOfSentence': ret = `${response.comeback} ${responseWord}`; break;
    case 'endOfSentence': ret = `${responseWord} ${response.comeback}`; break;
    default: break;
  }

  return ret;
}

function mapPersonalizedComeback() {
  if (!personalizedReplyStack.length)
    personalizedReplyStack = deepCopy(HIVONISMS.personalizedSayings);

  personalizedReplyStack = stir(personalizedReplyStack);
  const replyData = personalizedReplyStack.pop();

  let username$ = username || 'Андреј';

  let ret;
  if (replyData.placeNameAt === 'startOfSentence')
    return `${username$} ${replyData.comeback}`;
  else return `${replyData.comeback} ${username$}`;
}

function mapComeback() {
  if (!genericReplyStack.length)
    genericReplyStack = deepCopy(HIVONISMS.fullSayings);

  genericReplyStack = stir(genericReplyStack);
  const ret = genericReplyStack.pop();

  return ret;
}

function onInput(event) {
  if (event)
    event.preventDefault();


  const chatInputEl = document.getElementById('chatinput');
  const userSentence = chatInputEl.value;
  const result = processInput(userSentence);

  messageHandler({
    from: 'user',
    message: userSentence,
  });

  chatInputEl.value = '';
  chatInputEl.setAttribute('disabled', true);

  setTimeout(() => {
    messageHandler({
      from: 'hivona',
      message: result,
    });
    chatInputEl.removeAttribute('disabled');
    chatInputEl.focus();
  }, (Math.random() * 1000 + 500));
}

function messageHandler(data) {
  chatStack.push(data);
  localStorage.setItem('hgpt-chatstack', JSON.stringify(chatStack));
  setupView();
}

function processInput(userSentence) {
  if (
    userSentence.toLowerCase().includes('зајче') ||
    userSentence.toLowerCase().includes('zajce') ||
    userSentence.toLowerCase().includes('zajche')
  )
    return 'Иу'

  for (let i = 0; i < partialComebacksTriggerWords.length; i++) {
    const triggerWord = partialComebacksTriggerWords[i];
    if (userSentence.toLowerCase().includes(triggerWord))
      return mapPartialComeback(triggerWord);
  }
  return Math.random() > .15 ? mapComeback() : mapPersonalizedComeback();
}

function hideNameInput() {
  const nameInputHolderEl = document.getElementById('nameinputholder');
  nameInputHolderEl.setAttribute('style', 'visibility: hidden')
}

function showChat() {
  const chatHolderEl = document.getElementById('chatholder');
  chatHolderEl.setAttribute('style', 'visibility: visible');
}

function setName() {
  const nameInputEl = document.getElementById('nameinput');
  username = nameInputEl.value;
  hideNameInput();
  showChat();
}



function setupView() {
  drawChat();
  document.getElementById('last-placeholder').scrollIntoView(false);
}

function drawChat() {
  const convoHolderEl = document.getElementById('conversationholder');
  let htmlStr = '';
  chatStack.forEach(convoItem => {
    const classStr = convoItem.from === 'user' ? 'chat-bubble user-message' : 'chat-bubble hgpt-message';
    htmlStr += `<div class="${classStr}">${convoItem.message}</div>`;
  });
  htmlStr += '<div id="last-placeholder" style="width: 100%; display: block; width: 100%; margin-bottom: -8px;"></div>';
  convoHolderEl.innerHTML = htmlStr;
}

function initUsername() {
  if (!!localStorage.getItem('hgpt-uname')) {
    username = localStorage.getItem('hgpt-uname')
    return;
  }
  username = prompt('Унесите лично име.', 'Naky')
  localStorage.setItem('hgpt-uname', username);
}


initStacks()
initUsername()
